package com.company;

public class Entrada {

    private String ID;
    private Zona zona;
    private String nombreComprador;
    private String descuento;

    public Entrada(String id, Zona zona, String nombreComprador, String descuento){

        this.ID = id;
        this.zona = zona;
        this.nombreComprador = nombreComprador;
        this.descuento = descuento;

    }

    public Zona getZona() {
        return zona;
    }

    public String getDescuento() {
        return descuento;
    }

    public String getID() {
        return ID;
    }

    public String getNombreComprador() {
        return nombreComprador;
    }

    @Override
    public String toString() {
        return "ID: "+ID+"\n"+"Zona: "+zona.getNombre()+"\n"+"Nombre comprador: "+nombreComprador+"\n"+"Tipo de descuento: "+descuento;
    }
}
