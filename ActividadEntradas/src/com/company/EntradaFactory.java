package com.company;

import java.util.ArrayList;
import java.util.Scanner;

public class EntradaFactory {

    private ArrayList<String> idsArray;
    private ArrayList<Entrada> entradaArrayList;


    public EntradaFactory(){

        this.idsArray = generateIds();
        this.entradaArrayList = new ArrayList<Entrada>();

    }

    private ArrayList<String> generateIds(){

        ArrayList<String> array = new ArrayList<String>();

        for (int i=0; i<100; i++){

            array.add(Integer.toString(i));

        }

        return array;

    }

    private String getId(){

        String id = this.idsArray.get(0);
        this.idsArray.remove(0);
        return id;

    }

    private Zona askZona(){

        Scanner scanner = new Scanner(System.in);

        System.out.println("Introduce la zona de la entrada");

        String zonaString = scanner.next();

        if (zonaString.equalsIgnoreCase("Central")){

            return new ZonaCentral();

        }else if (zonaString.equalsIgnoreCase("Lateral")){

            return new ZonaLateral();

        }else if (zonaString.equalsIgnoreCase("Principal")){

            return new ZonaPrincipal();

        }else if (zonaString.equalsIgnoreCase("Palco")){

            return new ZonaPalco();

        }else return null;

    }

    private String askDescuento(){

        Scanner scanner = new Scanner(System.in);

        System.out.println("Introduce el tipo de descuento");

        String descuento = scanner.next();

        if (descuento.equalsIgnoreCase("Normal")){
            return "normal";
        }else if (descuento.equalsIgnoreCase("Abonado")){
            return "abonado";
        }else if (descuento.equalsIgnoreCase("Reducida")){
            return "reducida";
        }else return null;

    }

    private Entrada askDatosEntrada(){

        Scanner scanner = new Scanner(System.in);

        Zona zonaAsked;

        String descuento;

        do {

            zonaAsked = askZona();

        }while (zonaAsked == null);

        do {

            descuento = askDescuento();

        }while (descuento == null);


        System.out.println("Introduce el nombre del comprador");

        String nombre = scanner.next();

        String id = getId();

        return new Entrada(id,zonaAsked,nombre,descuento);

    }

    private void venderEntrada(){



        double precioEntrada;
        String id;

        Entrada entrada = askDatosEntrada();

        if (!checkIfSeatsFull(entrada)){

            if (entrada.getDescuento().equalsIgnoreCase("Normal")){

                precioEntrada = entrada.getZona().getPrecioNormal();
                id = getId();
                System.out.println("Entrada vendida correctamente");
                System.out.println("ID de la entrada: "+id);
                System.out.println("El precio de la entrada es de "+precioEntrada);
                entrada.getZona().ocuparUnAsiento();
                entradaArrayList.add(entrada);
                return;
                //return true;
            }else if (entrada.getDescuento().equalsIgnoreCase("Abonado")){

                precioEntrada = entrada.getZona().getPrecioAbonado();
                id = getId();
                System.out.println("Entrada vendida correctamente");
                System.out.println("ID de la entrada: "+id);
                System.out.println("El precio de la entrada es de "+precioEntrada);
                entrada.getZona().ocuparUnAsiento();
                entradaArrayList.add(entrada);
                return;
                //return true;


            }else {

                precioEntrada = entrada.getZona().getPrecioReducido();
                id = getId();
                System.out.println("Entrada vendida correctamente");
                System.out.println("ID de la entrada: "+id);
                System.out.println("El precio de la entrada es de "+precioEntrada);
                entrada.getZona().ocuparUnAsiento();
                entradaArrayList.add(entrada);
                return;
                //return true;


            }

        }

        System.out.println("Zona llena - ERROR");
        //return false;
    }

    private boolean checkIfSeatsFull(Entrada entrada){

        if (entrada.getZona().getNumAsientos()>0){

            return false;

        }else {

            return true;

        }

    }

    private void consultarEntradaPorId(int id){

        for (int i=0; i<entradaArrayList.size(); i++){

            if (id==Integer.parseInt(entradaArrayList.get(i).getID())){
                System.out.println("Entrada encontrada, mostrando datos...");
                System.out.println(entradaArrayList.get(i));
                return;
            }

        }

        System.out.println("No hay ninguna entrada con esa ID");
    }

    private void consultaZona(String zona){

        double dineroRecaudado = 0;
        int numAsientosVendidos = 0;


        for (int i=0; i<entradaArrayList.size(); i++){

            if (entradaArrayList.get(i).getZona().getNombre().equalsIgnoreCase(zona)){

                numAsientosVendidos++;

                if (entradaArrayList.get(i).getDescuento().equalsIgnoreCase("normal")){

                    dineroRecaudado+=entradaArrayList.get(i).getZona().getPrecioNormal();

                }else if (entradaArrayList.get(i).getDescuento().equalsIgnoreCase("reducida")){

                    dineroRecaudado+=entradaArrayList.get(i).getZona().getPrecioReducido();

                }else if (entradaArrayList.get(i).getDescuento().equalsIgnoreCase("abonado")){

                    dineroRecaudado+=entradaArrayList.get(i).getZona().getPrecioAbonado();

                }

            }
        }

        System.out.println("Dinero recaudado: "+dineroRecaudado);
        System.out.println("Numero de asientos vendidos: "+numAsientosVendidos);

    }

    public void StartMenu(){

        Scanner scanner = new Scanner(System.in);

        int eleccion;

        do {


            System.out.println("Iniciando menu...");
            System.out.println("1 - Vender entrada");
            System.out.println("2 - Consultar datos de entrada");
            System.out.println("3 - Informe de zona");
            System.out.println("4 - Salir");

            eleccion = scanner.nextInt();

            if (eleccion == 1) {

                venderEntrada();

            } else if (eleccion == 2) {

                System.out.println("Introduce el id de la entrada");
                int id = scanner.nextInt();
                consultarEntradaPorId(id);

            } else if (eleccion == 3) {

                System.out.println("Introduce la zona de la que quieres obtener información");
                String zona = scanner.next();
                consultaZona(zona);


            } else {

                System.out.println("Introduce una opcion valida");

            }

        }while (eleccion!=4);

    }

}
