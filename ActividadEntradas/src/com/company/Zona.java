package com.company;

abstract public class Zona {

    private String nombre;
    private int numAsientos;
    private double precioNormal;
    private double precioAbonado;

    public Zona(String nombre, int numAsientos, double precioNormal, double precioAbonado){
        this.nombre=nombre;
        this.numAsientos=numAsientos;
        this.precioNormal=precioNormal;
        this.precioAbonado=precioAbonado;
    }


    public double getPrecioAbonado() {
        return precioAbonado;
    }

    public double getPrecioNormal() {
        return precioNormal;
    }

    public double getPrecioReducido(){

        double precioEntradaNormal = this.precioNormal;
        double precioReducido = (precioEntradaNormal * 15)/100;

        return precioEntradaNormal-precioReducido;

    }

    public int getNumAsientos() {
        return numAsientos;
    }

    public void ocuparUnAsiento(){
        numAsientos--;
    }

    public void restarEntradaVendida(){
        numAsientos--;
    }

    public String getNombre() {
        return nombre;
    }


}
