package com.poquet;

public class Bicicleta implements IReparable {

    private String numBastidor;
    private String modelo;
    private String marca;
    private int numRuedas;

    public Bicicleta(String numBastidor, String modelo, String marca, int numRuedas){

        this.numBastidor=numBastidor;
        this.modelo=modelo;
        this.marca=marca;
        this.numRuedas=numRuedas;

    }

    @Override
    public String getID() {
        return this.numBastidor;
    }

    @Override
    public String toString() {
        return "Bicicleta -> NumBastidor : "+this.numBastidor+" Modelo: "+this.modelo+" Marca: "+this.marca+" numRuedas: "+numRuedas;
    }
}
