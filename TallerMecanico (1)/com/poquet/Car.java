package com.poquet;

public class Car implements IReparable {

    private String matricula;
    private String color;
    private String modelo;
    private String marca;
    private String cilindrada;
    private int puertas;

    public Car(String matricula, String color, String modelo, String marca, String cilindrada, int puertas) {

        this.matricula = matricula;
        this.color = color;
        this.modelo = modelo;
        this.marca = marca;
        this.cilindrada = cilindrada;
        this.puertas = puertas;

    }

    public String getMatricula() {
        return matricula;
    }

    public String getID(){

        return this.matricula;

    }

    @Override
    public String toString() {
        return "Car{" +
                ", matricula='" + matricula + '\'' +
                ", color='" + color + '\'' +
                ", modelo='" + modelo + '\'' +
                ", marca='" + marca + '\'' +
                ", cilindrada='" + cilindrada + '\'' +
                ", puertas=" + puertas +
                '}';
    }
}
