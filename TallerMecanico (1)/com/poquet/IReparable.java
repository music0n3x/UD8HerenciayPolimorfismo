package com.poquet;

public interface IReparable {

    String getID();

}
