package com.poquet;

public class Main {

    public static void main(String[] args) {

        Taller taller = new Taller();
        Bicicleta bicicleta = new Bicicleta("abcd","modeloBici","marcaBici",3);
        Car coche = new Car("matricula SV","rojo","modeloCoche","marcaCoche","120CV",4);

        taller.registrarVehiculo(bicicleta);
        taller.listarPendientes();
        taller.registrarVehiculo(coche);
        taller.listarPendientes();

    }
}
