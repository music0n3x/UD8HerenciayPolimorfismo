package com.poquet;

import java.util.Scanner;

public class Menu {

    private Taller taller;

    private Scanner scannerInput;

    private final int OPTION_REGISTER = 1;
    private final int OPTION_LIST_FIXED = 2;
    private final int OPTION_LIST_PENDING = 3;
    private final int OPTION_ADD_FIXED = 4;
    private final int OPTION_TAKE_BACK = 5;
    private final int OPTION_EXIT = 10;

    public Menu(){

        this.taller = new Taller();
        this.scannerInput = new Scanner(System.in);

    }

    public void launch() {

        int option;
        do {

           option = this.getUserOption();

           if(option == this.OPTION_ADD_FIXED){

               this.registrarReparación();

           }else if(option == this.OPTION_LIST_FIXED){

               this.verListadoReparados();

           }else if(option == this.OPTION_LIST_PENDING){

               this.verListadoPendientes();

           }else if(option == this.OPTION_REGISTER){

               this.registrarNuevoCoche();

           }else if(option == this.OPTION_TAKE_BACK){

               this.retirarVehiculo();
           }

        }while (option != this.OPTION_EXIT);

    }

    private int getUserOption() {

        System.out.println(this.OPTION_REGISTER+".Registrar vehiculo");
        System.out.println(this.OPTION_LIST_FIXED+".Listar arreglados");
        System.out.println(this.OPTION_LIST_PENDING+".Listar pendientes");
        System.out.println(this.OPTION_ADD_FIXED+".Registrar arreglo");
        System.out.println(this.OPTION_TAKE_BACK+".Retirar vehiculo");
        System.out.println(this.OPTION_EXIT+".Salir");
        return this.scannerInput.nextInt();

    }

    private void registrarNuevoCoche(){

        System.out.println("Introduce la matricula");
        String matricula = this.scannerInput.next();
        System.out.println("Introduce el color");
        String color = this.scannerInput.next();
        System.out.println("Introduce el modelo");
        String modelo = this.scannerInput.next();
        System.out.println("Introduce la marca");
        String marca = this.scannerInput.next();
        System.out.println("Introduce la cilindrada");
        String cilindrada = this.scannerInput.next();
        System.out.println("Introduce el numero de puertas");
        int puertas = this.scannerInput.nextInt();

        Car newCar = new Car(matricula,color,modelo,marca,cilindrada,puertas);
        this.taller.registrarVehiculo(newCar);

    }

    private void registrarReparación(){

        this.taller.listarPendientes();
        System.out.println("¿Que coches ha reparado (introducir matricula)?");
        String matricula = this.scannerInput.next();
        this.taller.registrarArreglo(matricula);

    }

    private void retirarVehiculo(){

        System.out.println("¿Que coche quiere retirar (Introduce matricula)?");
        String matricula = scannerInput.next();

        if (this.taller.retirarVehiculo(matricula) != null) {

            System.out.println("Coche retirado con matricula "+matricula);

        }else{

            System.out.println("No existe coche pendiente de retirar con matricula "+matricula);
        }

    }

    private void verListadoReparados(){

        this.taller.listarReparados();

    }

    private void verListadoPendientes(){

        this.taller.listarPendientes();

    }

}
