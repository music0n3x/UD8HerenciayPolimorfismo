package com.poquet;

import java.util.ArrayList;

public class Taller {

    private ArrayList<IReparable> listaPendientes;

    private ArrayList<IReparable> listaReparados;

    public Taller(){

        this.listaPendientes = new ArrayList<>();
        this.listaReparados = new ArrayList<>();

    }

    public void registrarVehiculo(IReparable vehiculo){

        this.listaPendientes.add(vehiculo);

    }

    public IReparable retirarVehiculo(String id){

        IReparable vehiculo = this.getFixedCar(id);

        if (vehiculo != null) {

            this.listaReparados.remove(vehiculo);

        }

        return vehiculo;

    }

    public void registrarArreglo(String ID){

        IReparable vehiculo = this.getPendingCar(ID);

        if (vehiculo != null) {

            this.listaPendientes.remove(vehiculo);
            this.listaReparados.add(vehiculo);

        }

    }

    public void listarReparados(){

        for (int i = 0; i < listaReparados.size(); i++) {

            IReparable vehiculo = listaReparados.get(i);
            System.out.println(vehiculo);

        }

    }

    public void listarPendientes(){

        for (int i = 0; i < listaPendientes.size(); i++) {

            IReparable vehiculo = listaPendientes.get(i);
            System.out.println(vehiculo);

        }

    }

    private IReparable getPendingCar(String ID ){

        for (int i = 0; i<listaPendientes.size(); i++){

            IReparable vehiculo = listaPendientes.get(i);

            if (vehiculo.getID().equalsIgnoreCase(ID)){

                return vehiculo;

            }

        }

        return null;

    }

    private IReparable getFixedCar(String ID ){

        for (int i = 0; i<listaReparados.size(); i++){

            IReparable vehiculo = listaReparados.get(i);

            if (vehiculo.getID().equalsIgnoreCase(ID)){

                return vehiculo;

            }

        }

        return null;

    }

}
